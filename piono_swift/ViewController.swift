//
//  ViewController.swift
//  piono_swift
//
//  Created by Anton Medvediev on 18/03/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    
    var player: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func pianoPressed(_ sender: UIButton) {
        playSound(sender.currentTitle!)
    }
    
    func playSound(_ notePressed: String){
        let url = Bundle.main.url(forResource: notePressed, withExtension: "mp3")
        player = try! AVAudioPlayer(contentsOf: url!)
        player.play()
    }
}


